 > **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4369 - Extensible Enterprise Solutions (Python/R data Analytics/Visualization)

## Alexander C. Boehm

### Assignment 1 Requirements:

*Four Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development installations
3. Questions
4. Bitbucket repo links:

    a) this assignment and

    b) this completed tutorial (bitbucketstationlocations)

#### README.mf file should include the following items:

- Screenshot of a1_tip_calculator
- Link to A1.ipynb file: [tip_calculator.ipynb](a1 tip calculator/tip_calculator.ipynb "A1 Jupyter notebook")
- git commands w/short descriptions

>This is a blockquote.
>
>This is the second paragraph in the blockquote
>
>#### Git commands w/short descriptions

1. git init - Create an empty git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git branch - List, create, or delete branches

#### Assignment Screenshots:

#### Screenshot of a1 _tip_calculator application running (IDLE):
![Python Installation Screenshot IDLE](img/a1_tip_calculator_idle.png "A1 IDLE screenshot")

#### Screenshot of a1_tip_calculator application running (Visual Studio Code:)
![Python Installation Screenshot VS Code](img/a1_tip_calculator_vs_code.png "A1 VS Code Screenshot")

#### A1 Jupyter Notebook:
![tip_calculator.ipynb](img/a1_juypter_notebook.png "A1 Jupyter Notebook")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/zombotastic/bitbucketstationlocations/ "Bitbucket Station Locations")