# LIS4369 - Extensible Enterprise Solutions (Python/R data Analytics/Visualization)

## Alexander C. Boehm

### A5 Requirements:

*Five Parts:*

1. Complete R tutorial
2. Code and run a5
3. Screenshots of the program and tutorial
4. R files
5. Skill sets Thirteen, Fourteen, and Fifteen

#### Screenshots of A5 graphs (R-Studio):

|![A5 R-studio](img/a5.png "A5 R-studio")|
|![A5 R-studio](img/a5b.png "A5 R-studio")|

#### Screenshots of Tutorial graphs (R-Studio):

|![Tutorial R-Studio](img/tutorialGraph2.png "Tutorial R-Studio")|
|![Tutorial R-Studio](img/tutorialGraph3.png "Tutorial R-Studio")|


#### Command list screenshot
|![Graph VS](img/commandscreenshot.png "Graph screenshot")|

#### Skills Sets
|Skill Set Thirteen | Skill Set Fourteen | 
---|---|---
|![SS13 VS Code](img/ss13.png "SS13 VS Code")|![SS14 VS Code](img/ss14.png "SS14 VS Code")|

#### Skill Set Fifteen
![SS15 VS Code](img/ss15.png "SS15 VS Code")

#### .R file links

[Tutorial .R file](r_tutorial/learn_to_use_r.R ".R file")
<br>
[A5 .R file](lis4369_a5.R ".R file")