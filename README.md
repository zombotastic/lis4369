> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4369 - Extensible Enterprise Solutions (Python/R data Analytics/Visualization)

## Alexander C. Boehm

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md")
    - Install Python
    - install R
    - Install R Studio
    - Install Visual Studio Code
    - Create *a1_tip_calculator* application
    - Create *a1_tip_calculator* Juypter Notebook
    - Provide screenshot of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md")
    - create *a2_payroll_calculator* application
    - Provide screenshots of completed app
    - Provide jupiter notebook link and screenshot
    - Provide screenshots of skill sets 1, 2, & 3

3. [A3 README.md](a3/README.md "My A3 README.md")
    - Create Payroll Calculator
    - Provide screenshot of completed programs
  
4. [P1 README.md](p1/README.md "My P1 README.md")
    - Install required packages
    - Backwards-engineer a program in python which generates and dispalys a data frame
    - Screenshots of the program
    - Jupyter notebook link
    - Skill sets seven, eight, and nine
  
5. [A4 README.md](a4/README.md "My A4 README.md")
    - Install required packages
    - Backwards-engineer a program in python which generates and dispalys a data frame
    - Screenshots of the program
    - Jupyter notebook link
    - Skill sets Ten, Eleven, and Twelve

6. [A5 README.md](a5/README.md "My A5 README.md")
    - Complete R tutorial
    - Code and run a5
    - Screenshots of the program and tutorial
    - R files
    - Skill sets Thirteen, Fourteen, and Fifteen

7.  [P2 README.md](p2/README.md "My P2 README.md")
    - Backwards-engineer a program in R studio which manipulates data from the MTCars Data Frame in various ways
    - Create two graphs
    - Provide screenshots and links

