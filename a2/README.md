# LIS4369 - Extensible Enterprise Solutions (Python/R data Analytics/Visualization)

## Alexander C. Boehm

### Assignment 2 Requirements:

*Four Parts:*

1. Backwards-engineer a program in python which includes two modules, takes in user input, and calculates payment due 
2. Screenshots of the program
3. Jupyter notebook link
4. Skill sets one, two, and three

#### Screenshots of a2 _payroll_calculator application running (IDLE):

|Without Overtime | With Overtime |
--- | ---
|![Payroll Calculator IDLE](img/a2_payroll_calculator1.png "A2 IDLE screenshot")|![Payroll Calculator IDLE](img/a2_payroll_calculator2.png "A2 IDLE screenshot")|

#### Screenshots of a2 _payroll_calculator application in Jupyter Notebook:

![Payroll Calculator Jupyter Notebook](img/jupyter_notebook_screenshot_1.png "A2 notebook")

![Payroll Calculator Jupyter Notebook](img/jupyter_notebook_screenshot_2.png "A2 notebook")

[Payroll Calculator Jupyter Notebook](a2_payroll_calculator.ipynb "A2 notebook file")

|Skill Set One | Skill Set Two | Skill Set Three |
---|---|---
|![SS1 VS Code](img/ss1img.png "SS1 VS Code")|![SS2 VS Code](img/ss2img.png "SS2 VS Code")|![SS3 VS Code](img/ss3img.png "SS3 VS Code")|