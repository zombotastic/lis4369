# LIS4369 - Extensible Enterprise Solutions (Python/R data Analytics/Visualization)

## Alexander C. Boehm

### Project 1 Requirements:

*Five Parts:*

1. Install required packages
2. Backwards-engineer a program in python which generates and dispalys a data frame
3. Screenshots of the program
4. Jupyter notebook link
5. Skill sets seven, eight, and nine

#### Screenshots of p1_data_analysis application running (VS):

|![Data Analysis VS](img/p1a.png "P1 screenshot")|
|![Data Analysis VS](img/p1b.png "P1 screenshot")|
|![Graph VS](img/graph.png "Graph screenshot")|


|Skill Set Seven | Skill Set Eight | Skill Set Nine
---|---|---|---
|![SS7 VS Code](img/ss7.png "SS7 VS Code")|![SS8 VS Code](img/ss8.png "SS8 VS Code")|![SS9 VS Code](img/ss9.png "SS9 VS Code")|

[Data Analyis Jupyter Notebook](p1_data_analysis/p1__data_analysis.ipynb "P1 notebook file")