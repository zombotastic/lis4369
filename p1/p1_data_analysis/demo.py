import datetime
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style
start = datetime.datetime(2010, 1, 1,)
end = datetime.datetime(2018, 10, 15)
df = pdr.DataReader("XOM", "yahoo", start, end)
print("\nPrint number of records: ")
print(df.columns)
print("\nPrint data frame: ")
print(df)
print("\nPrint first five lines")
style.use('ggplot')

df['High'].plot()
df['Adj Close'].plot()
plt.legend()
plt.show()
