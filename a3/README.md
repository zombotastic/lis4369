# LIS4369 - Extensible Enterprise Solutions (Python/R data Analytics/Visualization)

## Alexander C. Boehm

### Assignment 3 Requirements:

*Four Parts:*

1. Backwards-engineer a program in python which includes two modules, takes in user input, and calculates paint costs
2. Screenshots of the program
3. Jupyter notebook link
4. Skill sets four, five, and six

#### Screenshots of a3 _painting_estimator application running (VS):

|![Painting Calculator VS](img/painting_estimator.png "A3 screenshot")|

|Skill Set Four | Skill Set Five | Skill Set Six | Skill Set Six
---|---|---|---
|![SS4 VS Code](img/ss4.png "SS4 VS Code")|![SS5 VS Code](img/ss5.png "SS5 VS Code")|![SS6 VS Code](img/ss6a.png "SS6 VS Code")||![SS6 VS Code](img/ss6b.png "SS6 VS Code")|

[Payroll Calculator Jupyter Notebook](img/a3_painting_calculator.ipynb "A3 notebook file")