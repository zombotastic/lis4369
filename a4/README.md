# LIS4369 - Extensible Enterprise Solutions (Python/R data Analytics/Visualization)

## Alexander C. Boehm

### A4 Requirements:

*Five Parts:*

1. Install required packages
2. Backwards-engineer a program in python which generates and dispalys a data frame
3. Screenshots of the program
4. Jupyter notebook link
5. Skill sets Ten, Eleven, and Twelve

#### Screenshots of A4_data_analysis_2 application running (VS):

|![Data Analysis 2 VS](img/a4_first.png "A4 screenshot")|
|![Data Analysis 2 VS](img/a4_4last.png "A4 screenshot")|
|![Data Analysis 2 VS](img/a4-3last.png "A4 screenshot")|
|![Data Analysis 2 VS](img/a4-2last.png "A4 screenshot")|
|![Data Analysis 2 VS](img/a4last.png "A4 screenshot")|

#### Graph created using MatPlotLib
|![Graph VS](img/graph.png "Graph screenshot")|

#### Skills Sets
|Skill Set Ten | Skill Set Eleven | Skill Set Twelve
---|---|---|---
|![SS10 VS Code](img/ss10.png "SS10 VS Code")|![SS11 VS Code](img/ss11.png "SS11 VS Code")|![SS12 VS Code](img/ss12.png "SS12 VS Code")|

[Data Analyis Jupyter Notebook](img/a4.ipynb "P1 notebook file")