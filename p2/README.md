# LIS4369 - Extensible Enterprise Solutions (Python/R data Analytics/Visualization)

## Alexander C. Boehm

### Project 2 Requirements:

*Three Parts:*

1. Backwards-engineer a program in R studio which manipulates data from the MTCars Data Frame in various ways
2. Create two graphs
3. Provide screenshots and links

#### Screenshots of lis4369_p2 in R Studio:

|![p2 R studio](img/p2a.png "p2 screenshot")|
|![p2 R studio](img/p2b.png "p2 screenshot")|
|![p2 R studio](img/p2c.png "p2 screenshot")|
|![p2 R studio](img/p2d.png "p2 screenshot")|


|Graph A | Graph B |
---|---
|![Graph-A R studio](img/p2grapha.png "Graph-A R studio]")|![Graph-A R studio](img/p2graphb.png "Graph-A R studio")|

[lis_4369 project two R-file](lis4369_p2.R "lis_4369 project two R-file")